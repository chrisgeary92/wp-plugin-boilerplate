<?php namespace Acme\Core;

class Plugin {

    /**
     * Holds a single instance of our Plugin class
     *
     * @var Plugin|null
     */
    protected static $instance = null;

    /**
     * Return the instance of our Plugin class
     *
     * @return Plugin|null
     */
    public static function getInstance()
    {
        if(is_null(self::$instance))
        {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Run the plugin
     *
     * @return void
     */
    public function run()
    {

    }
} 