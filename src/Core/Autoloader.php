<?php namespace Acme\Core;

/**
 * Paulyg/Autoloader - A slightly different PSR-0 and PSR-4 compatable class autoloader.
 *
 * Copyright 2012-2013 Paul Garvin <paul@paulgarvin.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
class Autoloader {

	/**
	 * @var array
	 */
	protected static $autoloaders = array();

	/**
	 * @param string $prefix
	 * @param string $path
	 * @param boolean $prepend
	*/
	public static function addPsr0($prefix, $path, $prepend = false)
	{
		$key = self::getHashKey($prefix, $path);
		
		$prefix = ltrim($prefix, '\\');
		$path = rtrim($path, DIRECTORY_SEPARATOR);
		
		$autoloader = function($fqcn) use ($prefix, $path)
		{
			if(0 !== strpos($fqcn, $prefix))
			{
				return;
			}
		
			$relpath = self::classToPath($fqcn);
			$file = $path . DIRECTORY_SEPARATOR . $relpath;
		
			if(is_file($file))
			{
				require $file;
			}
		};
		
		self::$autoloaders[$key] = $autoloader;
		
		spl_autoload_register($autoloader, true, $prepend);
	}

	/**
	 * @param $prefix
	 * @param $path
	 * @param boolean $prepend
	*/
	public static function addPsr4($prefix, $path, $prepend = false)
	{
		$key = self::getHashKey($prefix, $path);
	
		$prefix = ltrim($prefix, '\\');
		$path = rtrim($path, DIRECTORY_SEPARATOR);
	
		$autoloader = function($fqcn) use ($prefix, $path)
		{
			if(0 !== strpos($fqcn, $prefix))
			{
				return;
			}
		
			$trimmed = substr($fqcn, strlen($prefix));
		
			$relpath = self::classToPath($trimmed);
			$file = $path . DIRECTORY_SEPARATOR . $relpath;
		
			if(is_file($file))
			{
				require $file;
			}
		};

		self::$autoloaders[$key] = $autoloader;
	
		spl_autoload_register($autoloader, true, $prepend);
	}

	/**
	 * @param $prefix
	 * @param $path
	 */
	public static function remove($prefix, $path)
	{
		$key = self::getHashKey($prefix, $path);
	
		if( ! isset(self::$autoloaders[$key]))
		{
			return;
		}
	
		$autoloader = self::$autoloaders[$key];
	
		spl_autoload_unregister($autoloader);
	
		unset(self::$autoloaders[$key]);
	}

	/**
	 * @param $fqcn
	 */
	public static function classToPath($fqcn)
	{
		$nspos = strrpos($fqcn, '\\');
	
		if($nspos !== false)
		{
			$ns = substr($fqcn, 0, $nspos);
			$ns = str_replace('\\', DIRECTORY_SEPARATOR, $ns);
		
			$cls = substr($fqcn, $nspos + 1);
			$cls = str_replace('_', DIRECTORY_SEPARATOR, $cls);
		
			$normalised = $ns . DIRECTORY_SEPARATOR . $cls;
		}
		else
		{
			$normalised = str_replace('_', DIRECTORY_SEPARATOR, $fqcn);
		}
	
		return $normalised . '.php';
	}

	/**
	 * @param $prefix
	 * @param $path
	 */
	protected static function getHashKey($prefix, $path)
	{
		return crc32($prefix . '-' . $path);
	}
}