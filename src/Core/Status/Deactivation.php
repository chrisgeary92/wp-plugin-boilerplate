<?php namespace Acme\Core\Status;

class Deactivation extends Status {
	
	/**
	 * Method to run during deactivation
	 *
	 * @return void
	 */
	public static function handle()
	{
		
	}
}