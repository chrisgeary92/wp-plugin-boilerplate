<?php

/*
 * Plugin Name: WP Plugin Boilerplate
 * Description: A WordPress plugin boilerplate for rapid development
 * Author: Christopher Geary
 * Author URI: http://crgeary.com/
 */

// Check if we are calling the file correctly
//
if( ! defined('ABSPATH'))
{
    exit;
}

// Define base paths for the plugin
//
define('ACME_PATH', untrailingslashit(plugin_dir_path(__FILE__)));
define('ACME_URL', untrailingslashit(plugin_dir_url(__FILE__)));

// Include the autoloader
//
require_once(ACME_PATH . '/src/Core/Autoloader.php');

// Add our src/ directory as PSR4 autoload
//
Acme\Core\Autoloader::addPsr4('Acme\\', ACME_PATH . '/src');

// Plugin activation & deactivation
//
register_activation_hook(__FILE__, array('Acme\Core\Status\Activation', 'handle'));
register_deactivation_hook(__FILE__, array('Acme\Core\Status\Deactivation', 'handle'));

// Run the core..
//
\Acme\Core\Plugin::getInstance()->run();